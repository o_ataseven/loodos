//
//  MovieDetailCell.swift
//  Loodos
//
//  Created by Özgü Ataseven on 22.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//


import UIKit

class MovieDetailCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
