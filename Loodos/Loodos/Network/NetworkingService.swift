//
//  NetworkingService.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import Foundation
import Alamofire

class NetworkingService{
    
    static let shared = NetworkingService()
    private init(){}
    
    private let apiKey = "68a2d074"
    private let basePath = "http://www.omdbapi.com/"
    
    func getOMDBMovies(stringQuery: String, completion: @escaping ([Movie]?)->()){
        
        let urlString = basePath + "?s=" + stringQuery + "&apikey=" + apiKey
        
        guard let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) else {return }
        
        guard let url = URL(string: encodedString) else {return }
        
        Alamofire.request(url).responseJSON { (response) in
            if let data = response.data{
                do {
                    let decoder = JSONDecoder()
                    let searchMovie = try decoder.decode(SearchMovie.self, from: data)
                    completion(searchMovie.Search)
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }
            
        }
    }
    
    
    func getOMDBMovieDetail(movieID: String, completion: @escaping (MovieDetail?)->()){
        
        let urlString = basePath + "?i=" + movieID + "&apikey=" + apiKey
        
        guard let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) else {return }
        
        guard let url = URL(string: encodedString) else {return }
        
        Alamofire.request(url).responseJSON { (response) in
            if let data = response.data{
                do {
                    let decoder = JSONDecoder()
                    let movieDetail = try decoder.decode(MovieDetail.self, from: data)
                    completion(movieDetail)
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }
            
        }
    }
}
