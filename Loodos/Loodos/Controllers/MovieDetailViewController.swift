//
//  MovieDetailViewController.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Firebase

class MovieDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate , NVActivityIndicatorViewable{
    
    @IBOutlet var mainTableView: UITableView!
    
    var headerView : MovieHeaderView!
    let headerHeight: CGFloat = 420
    let minimumHeaderHeight: CGFloat = 100
    var imdbMovieID: String!
    var isScrolled = false
    
    private var movieDetailVM: MovieDetailViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
        mainTableView.tableFooterView = UIView()
        
        setupUI()
    }
    
    
    func setupUI(){
        
        title = "Movie Detail"
        
        let backButton = UIBarButtonItem()
        backButton.title = "Back"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        headerView = Bundle.main.loadNibNamed("MovieHeaderView", owner: self, options: nil)?[0] as? MovieHeaderView
        view.addSubview(headerView)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        fetchMovieDetail()
    }
    
    func fetchMovieDetail(){
        
        startProgress()
        
        NetworkingService.shared.getOMDBMovieDetail(movieID: self.imdbMovieID) { (detail) in
            
            self.stopAnimating()
            
            if let movieDetail = detail{
                self.movieDetailVM = MovieDetailViewModel(movieDetail)
                
                guard let detailVM = self.movieDetailVM else { return }

                self.headerView.headerImage?.sd_setImage(with: URL(string: detailVM.poster)) { (im, error, _, _) in
                    self.headerView.setNeedsLayout()
                    
                }
                
                
                self.sendAnalytics(detailVM: detailVM)
                
                DispatchQueue.main.async {
                    self.mainTableView.reloadData()
                }
            }
        }
        
    }
    
    func startProgress(){
        
        let size = CGSize(width: 50, height: 50)
        startAnimating(size, message: "Loading", messageFont: UIFont(name: "HelveticaNeue-Bold", size: 17), type: .ballScaleMultiple, color: #colorLiteral(red: 0.231372549, green: 0.6862745098, blue: 0.7490196078, alpha: 1), padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 0, backgroundColor: #colorLiteral(red: 0.7889831853, green: 0.7889831853, blue: 0.7889831853, alpha: 0.3622645548), textColor: #colorLiteral(red: 0.231372549, green: 0.6862745098, blue: 0.7490196078, alpha: 1), fadeInAnimation: nil)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !isScrolled{
            headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: headerHeight)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieDetailVM?.numberOfRowsInSections(section) ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "movieDetailCell", for: indexPath) as? MovieDetailCell else {
            fatalError("Cell is not found")
        }
        
        if let title =  self.movieDetailVM?.movieDetailTitles[indexPath.row]{
            cell.titleLabel.text = title
        }
        
        if let desc = self.movieDetailVM?.movieDetailValues[indexPath.row]{
            cell.descriptionLabel.text = desc
        }
    
        return cell
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard let _ = headerView else {
            return
        }
        
        let y = headerHeight - (scrollView.contentOffset.y + headerHeight)
        let height = min(max(y, CGFloat(minimumHeaderHeight)), headerHeight + minimumHeaderHeight) 
        
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrolled = true
    }
    
    
    func sendAnalytics(detailVM: MovieDetailViewModel){
        
        var dict: [String: String] = [:]
        
        let parameterCount = detailVM.movieDetailTitles.count - 1
        
        for index in 0...parameterCount{
            
            let trimToCharacter = 100
            let shortString = String(detailVM.movieDetailValues[index].prefix(trimToCharacter))
            
            let key: String = detailVM.movieDetailTitles[index].lowercased().replacingOccurrences(of: " ", with: "_")
            
            dict[String(key.dropLast())] = shortString
          
        }
        
        Analytics.logEvent("movie_log", parameters: dict)
        
    }
    
}
