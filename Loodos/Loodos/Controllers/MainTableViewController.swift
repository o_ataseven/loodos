//
//  MainViewController.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView

class MainTableViewController: UITableViewController, UISearchBarDelegate , NVActivityIndicatorViewable {
    
    var titleText: String?
    private var movieListVM: MovieListViewModel?
    var selectedImdbID: String?
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        fetchMovies(string: "Matrix")
       
    }
    
    func fetchMovies(string: String){
        
        startProgress()
        
        NetworkingService.shared.getOMDBMovies(stringQuery: string) {movies in
            
            self.stopAnimating()
            
            if let allMovies = movies{
                self.movieListVM = MovieListViewModel(movies: allMovies)
            } else {
                self.movieListVM = MovieListViewModel(movies: [])
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let rowCount = self.movieListVM?.numberOfRowsInSections(section){
            if rowCount == 0 {
                tableView.setEmptyView(title: "No Result Found...", message: "Plase Look For Another Movie")
            } else {
                tableView.restore()
            }
            return rowCount
        } else {
            return 0
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.movieListVM?.numberOfSections ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as? MovieViewCell else {
            fatalError("Cell is not found")
        }
        
        if let movieVM =  self.movieListVM?.movieAtIndex(indexPath.row){
            
            cell.movieTitleLabel.text = movieVM.title
            cell.movieIDLabel.text = movieVM.imdbId
            cell.movieTypeLabel.text = movieVM.movieType
            cell.movieYearLabel.text = movieVM.year
            
            if let imageUrl = URL(string: movieVM.poster){
                cell.movieImageView.sd_setImage(with: imageUrl) { (_, err, _, _) in
                    if err != nil {
                        cell.noImageLabel.isHidden = false
                    } else {
                        cell.noImageLabel.isHidden = true
                    }
                }
            } else {
                cell.noImageLabel.isHidden = false
            }
            
        }
        return cell
    }
    
    func startProgress(){
        
        let size = CGSize(width: 50, height: 50)
        
        startAnimating(size, message: "Loading", messageFont: UIFont(name: "HelveticaNeue-Bold", size: 17), type: NVActivityIndicatorType.ballScaleMultiple, color: #colorLiteral(red: 0.231372549, green: 0.6862745098, blue: 0.7490196078, alpha: 1), padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 0, backgroundColor: #colorLiteral(red: 0.7889831853, green: 0.7889831853, blue: 0.7889831853, alpha: 0.3622645548), textColor: #colorLiteral(red: 0.231372549, green: 0.6862745098, blue: 0.7490196078, alpha: 1), fadeInAnimation: nil)
    }
    
    
    func setupUI(){
        
        self.navigationController?.navigationBar.isTranslucent = false
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
        
        self.navigationItem.searchController = searchController
        
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.foregroundColor : UIColor.white,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        
        title = titleText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text, searchText.count != 0 {
            fetchMovies(string: searchText)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedImdbID = self.movieListVM?.movieAtIndex(indexPath.row).imdbId
        self.performSegue(withIdentifier: "toMovieDetail", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMovieDetail" {
            if let detailVC = segue.destination as? MovieDetailViewController {
                detailVC.imdbMovieID = self.selectedImdbID
            }
            
        }
    }

}



