//
//  SplashViewController.swift
//  Loodos//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import UIKit
import Reachability
import Firebase

class SplashViewController: UIViewController {
    
    let reachability = Reachability()!
    @IBOutlet var noConnView: UIView!
    @IBOutlet var noConnViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var splasLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkConnection()
    }
    
    private func checkConnection(){
        
        if reachability.connection == .none{
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
                self.noConnViewBottomConstraint.constant = 40
                self.view.layoutSubviews()
            }, completion: nil)
            
        } else { 
            
            fetchRemoteConfig()
        }
    }
    
    
    private func fetchRemoteConfig(){
        
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: 0) { (status, err) in
            
            guard err == nil else{ return }
            
            RemoteConfig.remoteConfig().activateFetched()
            
            if let splashLabelText = RemoteConfig.remoteConfig().configValue(forKey: "splashText").stringValue{ 
                self.splasLabel.text = splashLabelText
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                    self.performSegue(withIdentifier: "toMain", sender: nil)
                })
                
            }
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let navigationController = segue.destination as? UINavigationController{
            if let mainVC = navigationController.topViewController as? MainTableViewController{
                mainVC.titleText = self.splasLabel.text
            }
        }
        
        
    }
    
    
    
    

}
