//
//  Movie.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import Foundation


struct SearchMovie: Codable {
    let Search: [Movie]
}


struct Movie: Codable {
    let title: String
    let year: String
    let imdbID: String
    let movieType: String
    let poster: String
    
    private enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbID
        case movieType = "Type"
        case poster = "Poster"
    }
}
