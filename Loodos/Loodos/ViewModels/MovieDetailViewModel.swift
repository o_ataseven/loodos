//
//  MovieDetailViewModel.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import Foundation

struct MovieDetailViewModel {
    private let movieDetail: MovieDetail
}

extension MovieDetailViewModel {
    
    var movieDetailTitles: [String] {
        var titleArray: [String] = []
        titleArray.append(title.0)
        titleArray.append(year.0)
        titleArray.append(rate.0)
        titleArray.append(released.0)
        titleArray.append(runTime.0)
        titleArray.append(genre.0)
        titleArray.append(director.0)
        titleArray.append(writer.0)
        titleArray.append(actors.0)
        titleArray.append(plot.0)
        titleArray.append(lang.0)
        titleArray.append(country.0)
        titleArray.append(awards.0)
        titleArray.append(imdbRating.0)
        titleArray.append(movieType.0)
        return titleArray
    }
    
    var movieDetailValues: [String] {
        var valueArray: [String] = []
        valueArray.append(title.1)
        valueArray.append(year.1)
        valueArray.append(rate.1)
        valueArray.append(released.1)
        valueArray.append(runTime.1)
        valueArray.append(genre.1)
        valueArray.append(director.1)
        valueArray.append(writer.1)
        valueArray.append(actors.1)
        valueArray.append(plot.1)
        valueArray.append(lang.1)
        valueArray.append(country.1)
        valueArray.append(awards.1)
        valueArray.append(imdbRating.1)
        valueArray.append(movieType.1)
        return valueArray
    }
    
    func numberOfRowsInSections(_ section: Int) -> Int{
        return self.movieDetailValues.count
    }
  
}

extension MovieDetailViewModel {
    init(_ movieDetail: MovieDetail) {
        self.movieDetail = movieDetail
    }
}

extension MovieDetailViewModel {
  
    
    var title: (String,String) {
        return ("Title:", self.movieDetail.title)
    }
    
    var year: (String,String) {
        return ("Year:", self.movieDetail.year)
    }
    
    var rate: (String,String) {
        return ("Rate:", self.movieDetail.rated)
    }
    
    var released: (String,String) {
        return ("Released:", self.movieDetail.released)
    }
    
    var runTime: (String,String) {
        return ("Run Time:",self.movieDetail.runtime)
    }
    
    var genre: (String,String) {
        return ("Genre:",self.movieDetail.genre)
    }
    
    var director: (String,String) {
        return ("Director:", self.movieDetail.director)
    }
    
    var writer: (String,String) {
        return ("Writer:",self.movieDetail.writer)
    }
    
    var actors: (String,String) {
        return ("Actors:",self.movieDetail.actors)
    }
    
    var plot: (String,String) {
        return ("Plot:",self.movieDetail.plot)
    }
    
    var lang: (String,String) {
        return ("Language:",self.movieDetail.language)
    }
    
    var country: (String,String) {
        return ("Country:",self.movieDetail.country)
    }
    
    var awards: (String,String) {
        return ("Awards:", self.movieDetail.awards)
    }
    
    var imdbRating: (String,String) {
        return ("IMDB Rate:",self.movieDetail.imdbRating)
    }
    
    var movieType: (String,String) {
        return ("Movie Type:", self.movieDetail.type)
    }
     
    var poster: String {
        return self.movieDetail.poster
    }
    
}

