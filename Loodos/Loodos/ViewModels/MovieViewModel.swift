//
//  MovieViewModel.swift
//  Loodos
//
//  Created by Özgü Ataseven on 21.08.2019.
//  Copyright © 2019 Özgü Ataseven. All rights reserved.
//

import Foundation


struct MovieListViewModel {
    let movies: [Movie]
}

extension MovieListViewModel {
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSections(_ section: Int) -> Int{
        return self.movies.count
    }
    
    func movieAtIndex(_ index: Int) -> MovieViewModel {
        let movie = self.movies[index]
        return MovieViewModel(movie)
    }
}



struct MovieViewModel {
    private let movie: Movie
}

extension MovieViewModel {
    init(_ movie: Movie) {
        self.movie = movie
    }
}

extension MovieViewModel {
    
    var title: String {
        return self.movie.title
    }
    
    var imdbId: String {
        return self.movie.imdbID
    }

    var movieType: String {
        return self.movie.movieType
    }
    
    var year: String {
        return self.movie.year
    }
    
    var poster: String {
        return self.movie.poster
    }
    
}

